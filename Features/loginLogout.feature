Feature: Login scenarios 

@Skim
Scenario: Verify Successful Login TestCaseIds 49293
	Given Navigate to Tsuruha "https://tsuruha-uat.couponnetwork.jp/" 
	When Navigate to Catalina Page 
	Then navigate to login page 
	Then Enter credentials and click on the Login Button

@Skim
Scenario Outline: Verify Unsuccessful Login attempts TestCaseIds 49293 
	Given Navigate to Tsuruha "<url>" 
	When Navigate to Catalina Page 
	Then navigate to login page 
	Then Enter  "<Email>" and  "test132" and click on the Login Button 
	Then Enter  "<Email>" and  "test123" and click on the Login Button 
	Examples: 
		|url                                  |         	Email 			|
		|https://tsuruha-uat.couponnetwork.jp/|tsuruhauat#yopmail.com		|
		|https://tsuruha-uat.couponnetwork.jp/|tsuruhauat1@yopmail.com		|

@Skim
Scenario: Verify header TestCaseIds 49227,49229
	Given Navigate to Tsuruha "https://tsuruha-uat.couponnetwork.jp/" 
	When Navigate to Catalina Page 
	Then Verify the Welcome Page 
	Then Enter credentials and click on the Login Button
	Then Verify Select Reward Page is displayed 
	
		@Skim
		Scenario: Verify My Page and Menu Items TestCaseIds 49230,49228
			Given Navigate to Tsuruha "https://tsuruha-uat.couponnetwork.jp/" 
			When Navigate to Catalina Page 
			Then Verify the Welcome Page 
			Then Verify the menu items for "Anonymous User" 
			Then Click on Site Logo and verify "Same Page" is displayed 
			Then Enter credentials and click on the Login Button 
			Then Verify the menu items for "Logged In User" 
			Then Click on Site Logo and verify "My Page" is displayed 
			
				
			@Skim
				Scenario: Reset Password TestCaseIds 49292
					Given Navigate to Tsuruha "https://tsuruha-uat.couponnetwork.jp/" 
					When Navigate to Catalina Page 
					Then Navigate to login page and click on forgot password link 
					Then Enter "tsuruhauat@yopmail.com" and reset password 
					Then Navigate to yopmail "http://www.yopmail.com/en/" and verify 
