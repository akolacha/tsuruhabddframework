Feature: Registration Scenarios 

@Skim
Scenario: : Registration in Tsuruha TestCaseIds 48371 
	Given Navigate to Tsuruha "https://tsuruha-uat.couponnetwork.jp/" 
	When Navigate to Catalina Page 
	Then navigate to Registration page 
	Then Enter email and password and click on membership button 
	Given Logout as user is logged in 
	Then Enter credential and verify login successful 
	Then Verify registration Successful 

@Skim
Scenario Outline: Edit User Details TestCaseIds 48695,48372 
	Given Navigate to Tsuruha "https://tsuruha-uat.couponnetwork.jp/" 
	When Navigate to Catalina Page 
	Then Verify the Welcome Page 
	Then Enter  "<Email>" and  "<password>" and click on the Login Button 
	Then Enter different combinations in the email field and verify 
	Then Enter different combination in the password field and verify 
	Then Enter "<newEmail>" and "<newPassword>" and update 
	Examples: 
		|         	Email 			    |				newEmail		|password|newPassword|
		|tsuruhauat1@yopmail.com	    |tsuruhaUpdateduat@yopmail.com	|test123 | test456   |
		|tsuruhaUpdateduat@yopmail.com	|	tsuruhauat1@yopmail.com     |test456 | test123   |
	 