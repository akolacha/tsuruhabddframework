Feature: Select a Store 

@Skim
Scenario: Select a Store by Zip code TestCaseIds 48380 
	Given Navigate to Tsuruha "https://tsuruha-uat.couponnetwork.jp/" 
	When Navigate to Catalina Page 
	Then navigate to login page 
	Then Enter credentials and click on the Login Button 
	Then Click on the zipcode icon 
	Then Enter zipcode "1400004" and search 
	Then Select a store and navigate to the store 

@Skim
Scenario: Select a Store by prefecture TestCaseIds 48380 
	Given Navigate to Tsuruha "https://tsuruha-uat.couponnetwork.jp/" 
	When Navigate to Catalina Page 
	Then navigate to login page 
	Then Enter credentials and click on the Login Button 
	Then Click on the prefecture icon 
	Then Select a store and navigate to the store 
	