package com.catalina.ImageComparator;


public interface CompareImage {
	public double compareImage(Image img, Image img2);
	GrayScaleConvertorImpl grayScaleConv=new GrayScaleConvertorImpl();
}
