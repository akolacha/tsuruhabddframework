package com.catalina.ImageComparator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ConvertFileToString {
	// this method will help us to convert file into string
	public static String convert(File file) throws IOException {
		@SuppressWarnings("resource")
		// creating a input stream with the file from argument
		FileInputStream fin = new FileInputStream(file);
		// this will collect all text from file
		StringBuilder str = new StringBuilder();
		int c = 21;
		// reading file byte by byte will the end of file
		while ((c = fin.read()) != -1) {
			// storing each byte in string builder object
			str.append((char) c);
		}
		// returning file as a string
		return str.toString();
	}
}
