package com.catalina.ImageComparator;

import java.util.Map;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ConvertJsonToString {

	public static String convert(JsonElement element)
	{
		return print(element, new StringBuilder()).toString();
	}
	private static StringBuilder print(JsonElement element,StringBuilder str) {
		if (element != null) {
			if (element.isJsonObject()) {
				Set<Map.Entry<String, JsonElement>> subElements = ((JsonObject) element).entrySet();
				str.append("{\n");
				int size = subElements.size();
				int index =0;
				for (Map.Entry<String, JsonElement> subElement : subElements) {
					index++;
					str.append("\""+subElement.getKey()+"\""+":");
					str.append(print(subElement.getValue(), new StringBuilder()));
					if(index<size)
						str.append(",\n");
					else
						str.append("\n");
				}
				str.append("}\n");
			} else if (element.isJsonArray()) {
				JsonArray array = element.getAsJsonArray();
				for (int i = 0; i < array.size(); i++) {
				}
			} else if (element.isJsonNull()) {
				str.append("null");
			} else if (element.isJsonPrimitive()) {
				try {
					int x = Integer.parseInt(element.getAsString());
					str.append(x);
				}
				catch (Exception e) {
					// TODO: handle exception
					try {
					double x = Double.parseDouble(element.getAsString());
					str.append(x);
					}
					catch(Exception ex)
					{
						try {
							Long x = Long.parseLong(element.getAsString());
							str.append(x);
						}
						catch(Exception exx)
						{
							if(element.getAsString().equalsIgnoreCase("true")||element.getAsString().equalsIgnoreCase("false"))
								str.append(element.getAsString());
							else
								str.append("\""+element.getAsString()+"\"");
						}
					}
				}
			}

		}
		return str;
	}
}
