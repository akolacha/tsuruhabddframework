package com.catalina.ImageComparator;


public interface Cropper {
	public Image cropImage(Image image, int x, int y, int width, int height);
}
