package com.catalina.ImageComparator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.unitils.thirdparty.org.apache.commons.io.FileUtils;

/**
 * 
 * @author automation
 *
 */
public class FileUtil {

	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */
	public static List<File> getFilesInFolder(String folderPath,
			String fileNamePattern) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);

		File files[] = folder.listFiles();
		if (files == null)
			return list;
		for (File file : files) {
			if (file.getName().contains(fileNamePattern))
				list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */
	public static List<File> getFilesInFolder(String folderPath,
			String fileNamePattern, String fileExtension) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return list;
		for (File file : files) {
			if (file.getName().contains(fileNamePattern)
					&& file.getName().contains(fileExtension))
				list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */

	/**
	 * 
	 * @param folderPath
	 */
	public static List<File> getFilesInFolder(String folderPath) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return list;
		for (File file : files) {
			list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param sourceFolderPath
	 * @param targetFolderPath
	 */
	public static boolean moveFilesToFolder(String sourceFolderPath,
			String targetFolderPath) {
		File folder = new File(sourceFolderPath);
		boolean flag = true;
		File files[] = folder.listFiles();
		if (files == null)
			return false;
		for (File file : files) {
			if (file.renameTo(new File(targetFolderPath + file.getName()))) {
				file.delete();
			} else {
				flag = false;
			}
		}
		return flag;
	}

	/**
	 * 
	 * @param sourceFolderPath
	 * @param targetFolderPath
	 * @param fileName
	 */
	public static boolean moveFilesToFolder(String sourceFolderPath,
			String targetFolderPath, String fileName) {
		File folder = new File(sourceFolderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return false;
		for (File file : files) {
			if (file.getName().contains(fileName))
				if (file.renameTo(new File(targetFolderPath + file.getName()))) {
					file.delete();
					return true;
				}
		}
		return false;
	}

	/**
	 * 
	 * @param folderPath
	 * @return
	 */
	public static boolean deleteFilesInFolder(String folderPath) {
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return false;
		try {
			for (File file : files)
				file.delete();
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static String fileTOString(File file) throws IOException {
		StringBuilder str = new StringBuilder();
		FileInputStream fin = new FileInputStream(file);
		int i = 0;
		while ((i = fin.read()) != -1) {
			str.append((char) i);
		}
		fin.close();
		fin = null;
		return str.toString();
	}

	/**
	 * This metjod returns the windows equivalent of the linux shared rive path
	 * 
	 * @param linuxPath
	 * @param windowsSharedDrive
	 * @return
	 */
	public static String getWindowsPath(String linuxPath,
			String windowsSharedDrive) {
		String tokens[] = linuxPath.split("/");

		StringBuffer buf = new StringBuffer();
		for (String token : tokens)
			if (!token.trim().isEmpty())
				buf.append("\\").append(token);

		return "\\\\" + windowsSharedDrive + buf;
	}

	/**
	 * @param destinationPath
	 * @param fileName
	 * @param file
	 */
	public static void copyFile(String destinationPath, String fileName,
			File file) throws IOException {
		FileUtils.copyFile(file, new File(destinationPath + fileName));
	}

	public static String getFileByPattern(String path, String pattern) {
		// TODO Auto-generated method stub
		Pattern pattern1 = Pattern.compile(pattern.replace("\\", "").replace("*", "[a-zA-Z0-9]*"));
		Matcher matcher = null;
		File folder = new File(path);
		String name =null;
		for(File file : folder.listFiles()){
			matcher = pattern1.matcher(file.getName());
			if(matcher.lookingAt())
				name = file.getName().substring(0, file.getName().lastIndexOf("."));
		}
		return "\\"+name;
	}

}
