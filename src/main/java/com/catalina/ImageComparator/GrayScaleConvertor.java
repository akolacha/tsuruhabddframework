package com.catalina.ImageComparator;


public interface GrayScaleConvertor {

	public Image convert(Image image);
}
