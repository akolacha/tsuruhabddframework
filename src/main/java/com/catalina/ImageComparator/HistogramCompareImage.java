package com.catalina.ImageComparator;

import java.awt.image.BufferedImage;
import java.util.Arrays;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.imgproc.Imgproc;


public class HistogramCompareImage implements CompareImage {
	@Override
	public double compareImage(Image img, Image img2) {
		Mat mat_1 = converToMAtImage(img);
		Mat mat_2 = converToMAtImage(img2);

		Mat hist_1 = new Mat();
		Mat hist_2 = new Mat();

		MatOfFloat ranges = new MatOfFloat(0f, 255f);
		MatOfInt histSize = new MatOfInt(25);

		Imgproc.calcHist(Arrays.asList(mat_1), new MatOfInt(0), new Mat(),
				hist_1, histSize, ranges);
		Imgproc.calcHist(Arrays.asList(mat_2), new MatOfInt(0), new Mat(),
				hist_2, histSize, ranges);
		double res = Imgproc
				.compareHist(hist_1, hist_2, Imgproc.CV_COMP_CORREL);
		
		System.out.println(res);
		Double diff = new Double(res * 100);
		
		return diff;
	}

	private Mat converToMAtImage(Image img) {
		
		BufferedImage bufferedImage = img.getImage();
		Mat src = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(), CvType.CV_8UC3);
		byte[] data = new byte[bufferedImage.getWidth() * bufferedImage.getHeight() * (int) src.elemSize()];
		int[] dataBuff = bufferedImage.getRGB(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), null, 0, bufferedImage.getWidth());
		for (int i = 0; i < dataBuff.length; i++) {
			data[i * 3] = (byte) ((dataBuff[i]));
			data[i * 3 + 1] = (byte) ((dataBuff[i]));
			data[i * 3 + 2] = (byte) ((dataBuff[i]));
		}
		src.put(0, 0, data);
		Mat mat1 = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(), CvType.CV_32S);
		//Imgproc.cvtColor(src, mat1, Imgproc.COLOR_RGB2HSV);

		return src;
	}

}
