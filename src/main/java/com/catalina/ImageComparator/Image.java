package com.catalina.ImageComparator;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Image {

	private BufferedImage image;

	public Image() {
		image = null;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public void readImage(String imageName, String imageExtension,
			String imagePath) {
		try {
			if (imageExtension.equals("png")) {
				BufferedImage image = ImageIO.read(new File(imagePath + "/"
						+ imageName + "." + imageExtension));
				BufferedImage result = new BufferedImage(image.getWidth(),
						image.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
				result.createGraphics().drawImage(image, 0, 0, Color.WHITE,
						null);
				setImage(result);
			} else {
				setImage(ImageIO.read(new File(imagePath + "/" + imageName
						+ "." + imageExtension)));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeImage(String imageName, String imageExtension,
			String imagePath) {
		try {
			ImageIO.write(this.image, imageExtension, new File(imagePath + "//"
					+ imageName + "." + imageExtension));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Image resize(Image img, int newW, int newH) {
		java.awt.Image tmp = img.getImage().getScaledInstance(newW, newH,
				java.awt.Image.SCALE_SMOOTH);
		BufferedImage dimg = new BufferedImage(newW, newH,
				BufferedImage.TYPE_3BYTE_BGR);

		Graphics2D g2d = dimg.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();
		img.setImage(dimg);
		return img;
	}
}
