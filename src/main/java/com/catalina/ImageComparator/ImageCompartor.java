package com.catalina.ImageComparator;

import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public abstract class ImageCompartor {

	private Factory factory = new Factory();

	public abstract boolean compareImageFromJson(ExtentTest test, Image img, Image img2, JsonObject mainJson);

	protected static int imageNO = 1;

	protected void logSideBySideImage(ExtentTest test, Image img, Image temp, String Operation) {
		// String image = saveImage(temp, "Before"+Operation);
		String greyImage = saveImage(img, Operation + "Image" + imageNO++);
		test.log(LogStatus.INFO,
				"<table><tr><th style=\"width:50%\">BEFORE</th><th style=\"width:50%\">After</th></tr><tr><td>"
						+ test.addScreenCapture("./img/Before" + Operation + (imageNO - 1) + ".jpg") + "</td><td>"
						+ test.addScreenCapture(greyImage) + "<td></tr></table>");

	}

	protected String saveImage(Image img, String imageName) {
		img.writeImage(imageName, "jpg", "./report/img/");
		return "./img/" + imageName + ".jpg";
	}

	protected boolean Image(ArrayList<JsonElement> Element, Image img, Image img2, ExtentTest test) {
		// TODO Auto-generated method stub
		int i = imageNO;
		boolean result = true;
		for (JsonElement element : Element) {
			JsonObject obj = (JsonObject) element;
			String startCord = obj.get("startCoord").getAsString();
			String endCord = obj.get("endCoord").getAsString();
			String startXY[] = startCord.split(",");
			String endXY[] = endCord.split(",");

			// Crop Image
			Cropper cropper = factory.getCropperInstance("normal");
			int x = Integer.parseInt(startXY[0]);
			int y = Integer.parseInt(startXY[1]);
			int height = Integer.parseInt(endXY[1]) - Integer.parseInt(startXY[1]);
			int width = Integer.parseInt(endXY[0]) - Integer.parseInt(startXY[0]);

			System.out.println("co ordination " + x + " " + y + " " + width + " " + height);

			Image temp = cropper.cropImage(img, x, y, width, height);
			Image temp2 = cropper.cropImage(img2, x, y, width, height);

			logCompareSideBySideImage(test, temp2, temp, "Image" + ++i);
			CompareImage ci = factory.getImageCompareInstance("histogram");
			temp.writeImage("abc", "jpg", "./report");
			temp2.writeImage("abc2", "jpg", "./report");
			double diff = ci.compareImage(temp, temp2);
			System.out.println(diff);
			if (diff < 98) {

				result &= false;
				ImageProcessingControllerWithMain.reportList.add("Image is not matched");
			}
			ImageProcessingControllerWithMain.reportList.add("Image is matched");
		}
		return result;
	}

	protected void logCompareSideBySideImage(ExtentTest test, Image temp2, Image temp, String Operation) {
		// TODO Auto-generated method stub
		String image = saveImage(temp, "" + Operation + "image1");
		String image2 = saveImage(temp2, Operation + "image2");
		test.log(LogStatus.INFO,
				"<table><tr><th style=\"width:50%\">First Image</th><th style=\"width:50%\">SecondImage</th></tr><tr><td>"
						+ test.addScreenCapture(image) + "</td><td>" + test.addScreenCapture(image2)
						+ "<td></tr></table>");

	}

	protected boolean comapreQRCodeValuesWithJson(Image img, JsonObject qrCode, ExtentTest test) {
		// TODO Auto-generated method stub
		if (qrCode == null)
			return true;
		Reader reader = factory.getReaderInstance("zing");
		String qrCodeTextFomImage = reader.read(img);
		String qrCodeFromJson = qrCode.get("message").getAsString();
		test.log(LogStatus.INFO, "Got value from image :- " + qrCodeTextFomImage);
		if (qrCodeFromJson.equals(qrCodeTextFomImage)) {
			test.log(LogStatus.INFO, "qrCode values are not matched");
			return true;
		}
		return false;
	}

	protected boolean checkMessages(Image img, boolean result, ArrayList<JsonElement> list, ExtentTest test) {
		int i = 0;
		for (JsonElement element : list) {
			JsonObject obj = (JsonObject) element;
			JsonArray messages = (JsonArray) obj.get("messageGroup");
			StringBuilder message = new StringBuilder();
			for (JsonElement str : messages)
				message.append(str.getAsString() + "\n");

			String startCord = obj.get("startCoord").getAsString();
			String endCord = obj.get("endCoord").getAsString();

			String startXY[] = startCord.split(",");
			String endXY[] = endCord.split(",");

			// Crop Image
			Cropper cropper = factory.getCropperInstance("normal");
			int x = Integer.parseInt(startXY[0]);
			int y = Integer.parseInt(startXY[1]);
			int height = Integer.parseInt(endXY[1]) - Integer.parseInt(startXY[1]);
			int width = Integer.parseInt(endXY[0]) - Integer.parseInt(startXY[0]);
			Image temp = cropper.cropImage(img, x, y, width, height);

			temp.writeImage("abcde", "jpg", "./report");
			// temp = Zoomer.zoomImage(temp, (float) 1.02);

			// TextSharper ts = new TextSharperImpl();
			// temp = ts.sharpeText(temp);

			// read text
			// logSideBySideImage(test, temp, img, "SubImage" + i);
			TextReader reader = factory.getTextReaderInstance("tessract");
			String text = reader.readText(temp);

			test.log(LogStatus.INFO,
					"Test Extracted<br> " + text + "<br>Compared With<br>" + message.toString().replace("\n", ""));

			if (!message.toString().replace("\n", "").equals(text.replace("\n", ""))) {
				test.log(LogStatus.FAIL, "Not able to match " + text.replace("\n", ""));
				result = false;
			}

		}

		return result;
	}

	protected boolean comapreBarCodeValuesWithJson(Image img, JsonObject barCode, ExtentTest test) {
		// TODO Auto-generated method stub
		boolean result = true;

		String type = barCode.get("type").getAsString();
		String barCodeValue = barCode.get("barMessage").getAsString();

		String imageBarCodeValue = "";
		Reader reader = factory.getReaderInstance("zing");
		switch (type) {
		case "UPC Code":
			imageBarCodeValue = reader.read(img);
			break;
		case "GSM1":
			break;
		}
		test.log(LogStatus.INFO, "Got value from image" + imageBarCodeValue);
		if (!barCodeValue.equals(imageBarCodeValue)) {
			test.log(LogStatus.FAIL, "Value are not matched");
			result = false;
		}
		test.log(LogStatus.INFO, "Value are matched");
		return result;
	}

	public boolean compareImage(Image img, Image img2) {
		boolean result = true;

		// convert to gray scale
		GrayScaleConvertor gsc = factory.getGrayScaleConveterInstance("normal");
		img = gsc.convert(img);
		img2 = gsc.convert(img2);

		// compare bar code values
		if (!comapreBarCodeValues(img, img2))
			result &= false;

		// Get Current direction
		String direction = "";
		int rotate = 0;
		switch (direction.toLowerCase()) {
		case "left":
			rotate++;
		case "up":
			rotate++;
		case "right":
			rotate++;
			break;
		}

		Rotater rotater = factory.getRotatorInstance("normal");
		// rotate image as per requirement
		for (int i = 0; i < rotate; i++) {
			try {
				img = rotater.rotate(img);
				img2 = rotater.rotate(img2);
			} catch (IOException e) {
				System.out.println(e);
			}
		}

		// get horizonal elements
		// ArrayList<JsonElement> horizonal = getElementData("horizontal");

		// get vertical elements
		// ArrayList<JsonElement> vertical = getElementData("vertical");

		return result;

	}

	protected ArrayList<JsonElement> getElementData(String string, JsonArray array) {
		// TODO Auto-generated method stub
		ArrayList<JsonElement> list = new ArrayList<JsonElement>();
		if (array == null)
			return list;
		for (JsonElement element : array)
			list.add(element);
		return list;
	}

	public boolean comapreBarCodeValues(Image img, Image img2) {
		// read barcode values
		Reader reader = factory.getReaderInstance("zing");
		String firstBarCode = reader.read(img);
		String secondBarCode = reader.read(img2);
		if (!firstBarCode.equals(secondBarCode)) {
			System.out.println("Barcode Not Matched");
			return false;
		}
		return true;

	}
}
