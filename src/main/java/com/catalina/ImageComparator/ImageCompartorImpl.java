package com.catalina.ImageComparator;

import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ImageCompartorImpl extends ImageCompartor {

	@Override
	public boolean compareImageFromJson(ExtentTest test,
			Image img,
			Image img2,
			JsonObject mainJson) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		Factory factory = new Factory();
		boolean result = true;

		// TODO: Add teh log with teh side by side image of before and after

		test.log(LogStatus.INFO, "Converting image to gray scale");
		// convert to gray scale

		img.writeImage("BeforeGrayScale"+imageNO, "jpg", "./report/img");
		Image temp = new Image();
		temp.readImage("BeforeGrayScale"+imageNO, "jpg", "./report/img");

		GrayScaleConvertor gsc = factory.getGrayScaleConveterInstance("normal");
		img = gsc.convert(img);
		img2 = gsc.convert(img2);
		logSideBySideImage(test, img, temp, "GrayScale");

		// TODO add the information read from the actual barcode
		test.log(LogStatus.INFO, "Reading barCode");
		JsonObject barCode = (JsonObject) mainJson.get("barCode");
		// compare bar code values
		if (barCode == null) {
			test.log(LogStatus.WARNING, "BarCode not found in json");
			ImageProcessingControllerWithMain.reportList.add("bar code is not present in image");
		}
		else if (!comapreBarCodeValuesWithJson(img, barCode, test)) {
			test.log(LogStatus.INFO, "Bar code values are not matched");
			result &= false;
			ImageProcessingControllerWithMain.reportList.add("bar code value is not matched");
		}
		ImageProcessingControllerWithMain.reportList.add("bar code value is matched");
		// TODO add the information read from the actual QR Code
		test.log(LogStatus.INFO, "Reading qrcode image");
		JsonObject qrCode = (JsonObject) mainJson.get("qrCode");
		// compare bar code values
		if (qrCode == null) {
			test.log(LogStatus.WARNING, "QrCode not found in json");
			ImageProcessingControllerWithMain.reportList.add("QR code is not present in image");
		}
		if (!comapreQRCodeValuesWithJson(img, qrCode, test)) {
			test.log(LogStatus.INFO, "qrCode is not matched");
			result &= false;
			ImageProcessingControllerWithMain.reportList.add("Qr code is not matched in image");
		}

		// TODO: Add the log with the side by side image of before and after
		temp = img;
		test.log(LogStatus.INFO, "Rotating a image");
		// Getting orientation of image
		String direction = mainJson.get("imageOrientation").getAsString();
		int rotate = 0;
		switch (direction.toLowerCase()) {
		case "left":
			rotate++;
		case "up":
			rotate++;
		case "right":
			rotate++;
			break;
		}

		img.writeImage("BeforeRotateImage"+imageNO, "jpg", "./report/img");
		temp = new Image();
		temp.readImage("BeforeRotateImage"+imageNO, "jpg", "./report/img");

		Rotater rotater = factory.getRotatorInstance("normal");
		// rotate image as per requirement
		for (int i = 0; i < rotate; i++) {
			try {
				img = rotater.rotate(img);
				img2 = rotater.rotate(img2);
			} catch (IOException e) {
				System.out.println(e);
			}
		}

		logSideBySideImage(test, img, temp, "RotateImage");

		test.log(LogStatus.INFO, "checking for the  images");

		// TODO: Add the log with the side by side image of before and after
		// Also can we mark the co-ordinates of teh image being checked in the
		// log. This
		// would be nice to have but we can work on this later
		ArrayList<JsonElement> Element = getElementData("ImageSections",
				(JsonArray) mainJson.get("staticImageSections"));
		if (Element == null || Element.size() == 0)
			test.log(LogStatus.INFO, "Dont have  image section");
		else {
			result &= Image(Element, img, img2, test);
			if (result)
				test.log(LogStatus.INFO, " images are matched");
			else
				test.log(LogStatus.INFO, " images are not matched");
		}

		
		
		
		// TODO: Add the log with the side by side image of before and after
		// get horizonal elements
		test.log(LogStatus.INFO, "Checking the horizontal messages");
		ArrayList<JsonElement> horizonal = getElementData("horizontal",
				(JsonArray) mainJson.get("horizontalMessages"));
		if (horizonal == null || horizonal.size() == 0)
			test.log(LogStatus.INFO, "Not elment found");
		else if (horizonal != null && horizonal.size() > 0) {
			result &= checkMessages(img2, result, horizonal, test);
			if (!result)
				test.log(LogStatus.INFO, "Horizontal messages are not matched");

		}

		// rotate image
		// TODO: Add the log with the side by side image of before and after
		// get vertical elements

		try {
			img = rotater.rotate(img);
			img2 = rotater.rotate(img2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		test.log(LogStatus.INFO, "Checking the vertical messages");
		ArrayList<JsonElement> vertical = getElementData("vertical",
				(JsonArray) mainJson.get("verticalMessages"));
		if (vertical == null || vertical.size() == 0)
			test.log(LogStatus.INFO, "Vertical messages are not presesnt");
		if (vertical != null && vertical.size() > 0) {
			result &= checkMessages(img2, result, vertical, test);
			if (!result)
				test.log(LogStatus.INFO, "Vertical messages are not matched");
		}
		
		

		// TODO: Add the log with the side by side image of before and after
		// Also can we mark the co-ordinates of teh image being checked in the
		// log. This
		// would be nice to have but we can work on this later
		ArrayList<JsonElement> Element1 = getElementData("ImageSections",
				(JsonArray) mainJson.get("staticImageSectionsVertical"));
		if (Element1 == null || Element1.size() == 0)
			test.log(LogStatus.INFO, "Dont have  image section");
		else {
			result &= Image(Element1, img, img2, test);
			if (result)
				test.log(LogStatus.INFO, " images are matched");
			else
				test.log(LogStatus.INFO, " images are not matched");
		}



		return result;
	}

}
