package com.catalina.ImageComparator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ImageProcessingControllerWithMain {

	public static List<String> reportList=null;
	
	
	public static void compare() throws IOException {

		ExtentReports report = new ExtentReports("report/ImageComparison.html");
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		List<File> files = FileUtil.getFilesInFolder(
				"./", ".json");
		for (File file : files)
			traverseJson(report, file);
		report.flush();
	}

	public static void traverseJson(ExtentReports report, File file)
			throws IOException {
		reportList = new ArrayList<>();
		ExtentTest test = report
				.startTest("Information of test files are going to be executed");


		// TODO: Log this entry - the name of teh JSON picked for comparison
		// read json file
		// String jsonFile =
		// "\\\\test_storage1\\spliceshare\\splice\\Coupon\\imageJson2.json";
		test.log(LogStatus.INFO,
				"Loading the file to execute code<br>" + file.getAbsolutePath());

		// File file = new File(jsonFile);
		String jsonContent = ConvertFileToString.convert(file);
		// parsing json file to objects
		JsonParser parser = new JsonParser();
		JsonObject mainJson = (JsonObject) parser.parse(jsonContent);

		// TODO: Log this entry
		// Lets add one more entry into teh JSON that tells us the expected
		// image name
		// in the /tmp/lane_* path.
		// This should indicate which image file should we pick up if multiple
		// image
		// files are generated
		// The image name can be a pattern as well. This will help us pick the
		// file if
		// the image name has a dynamic aspect to it
		// this file is referenced file
		JsonElement imagePath = mainJson.get("imagePathSource");
		String typeOfFile = mainJson.get("type").getAsString();
		// extract componenets
		String path = imagePath.getAsString().substring(0,
				imagePath.getAsString().lastIndexOf("\\"));
		String imageName = imagePath.getAsString().substring(
				imagePath.getAsString().lastIndexOf("\\"),
				imagePath.getAsString().lastIndexOf("."));
		String extension = imagePath.getAsString().substring(
				imagePath.getAsString().lastIndexOf(".") + 1);

		// TODO: Please change the code to pick the dynamic image created in the
		// /tmp/lane_*
		// read images
		// first image
		
		Image img = new Image();
		img.readImage(imageName, extension, path);

		imagePath = mainJson.get("imagePathTarget");

		// extract componenets
		path = imagePath.getAsString().substring(0,
				imagePath.getAsString().lastIndexOf("\\"));
		imageName = "";
		extension = imagePath.getAsString().substring(
				imagePath.getAsString().lastIndexOf(".") + 1);

		// this image is gennerated by coup or webpos
		// getting file name depending on the type
		// in case of pattern it will pick last file in the pattern
		imageName = findFileByType(imagePath, typeOfFile, path);

		// second image
		System.out.println(path + " " + imageName + " " + extension);
		Image img2 = new Image();
		img2.readImage(imageName, extension, path);

		report.endTest(test);
		test = report.startTest(imageName);
		// TODO: For logging part create separate ExtentTest for each type of
		// comparision. We can expand it as and when we grow further
		// comapare image
		// if (!compareImage(img, img2))
		// return;
		ImageCompartor imageCompartor = new ImageCompartorImpl();
		if (!imageCompartor.compareImageFromJson(test, img, img2, mainJson)) {
			// return false;
			test.log(LogStatus.FAIL, "Test fail");
		} else {
			test.log(LogStatus.PASS, "Test pass");
		}

		
		 /* GrayScaleConvertor gsc = new GrayScaleConvertor();
		  img = gsc.convertColorToGray(img); img2 = gsc.convertColorToGray(img2);
		  
		  CompareImage compare = new CompareImage(); double diff =
		  compare.compareImage(img, img2);
		  
		  test.log(LogStatus.INFO,"Similarity in images is " + diff +"%" );*/
		 
		// return true;
		report.endTest(test);
	}

	private static String findFileByType(JsonElement imagePath,
			String typeOfFile, String path) {
		String name = null;
		switch (typeOfFile.toLowerCase()) {
		case "pattern":
			name = FileUtil.getFileByPattern(
					path,
					imagePath.getAsString().substring(
							imagePath.getAsString().lastIndexOf("\\"),
							imagePath.getAsString().lastIndexOf(".")));
			break;
		case "absolute":
			name = imagePath.getAsString().substring(
					imagePath.getAsString().lastIndexOf("\\"),
					imagePath.getAsString().lastIndexOf("."));
			break;
		}
		return name;
	}

}
