package com.catalina.ImageComparator;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class ImageProcessor {

	public void convertColorImageToGrayscaleImage(String inputFile,
			String outputFile) {
		try {
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
			File input = new File(inputFile);
			BufferedImage image = ImageIO.read(input);

			byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer())
					.getData();
			Mat mat = new Mat(image.getHeight(), image.getWidth(),
					CvType.CV_8UC3);
			mat.put(0, 0, data);

			Mat mat1 = new Mat(image.getHeight(), image.getWidth(),
					CvType.CV_8UC1);
			Imgproc.cvtColor(mat, mat1, Imgproc.COLOR_RGB2GRAY);

			byte[] data1 = new byte[mat1.rows() * mat1.cols()
					* (int) (mat1.elemSize())];
			mat1.get(0, 0, data1);
			BufferedImage image1 = new BufferedImage(mat1.cols(), mat1.rows(),
					BufferedImage.TYPE_BYTE_GRAY);
			image1.getRaster().setDataElements(0, 0, mat1.cols(), mat1.rows(),
					data1);

			File ouptut = new File(outputFile);
			ImageIO.write(image1, "png", ouptut);

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void convertColorImageToSharpImage(String inputFile,
			String outputFile) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		Mat source = Imgcodecs.imread(inputFile);
		Mat destination = new Mat(source.rows(), source.cols(), source.type());

		Imgproc.GaussianBlur(source, destination, new Size(0, 0), 10);
		Core.addWeighted(source, 1.5, destination, -0.5, 0, destination);

		Imgcodecs.imwrite(outputFile, destination);
	}

	public boolean findChildImageInParentImage(Image childImage,
			Image parentImage) {
		try {
			PixelGrabber grab1 = new PixelGrabber(childImage.getImage(), 0, 0,
					-1, -1, false);
			PixelGrabber grab2 = new PixelGrabber(parentImage.getImage(), 0, 0,
					-1, -1, false);

			int[] data1 = null;

			if (grab1.grabPixels()) {
				int width = grab1.getWidth();
				int height = grab1.getHeight();
				data1 = new int[width * height];
				data1 = (int[]) grab1.getPixels();
			}

			int[] data2 = null;

			if (grab2.grabPixels()) {
				int width = grab2.getWidth();
				int height = grab2.getHeight();
				data2 = new int[width * height];
				data2 = (int[]) grab2.getPixels();
			}

			int j = 0;
			for (int i = 0; i < data2.length; i++) {
				if (data1[j] == data2[i]) {
					j++;
				}
				if (j == data1.length) {
					return true;
				}
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		return false;
	}

	public Image cutIntoSubImage(Image image, int startX, int startY,
			int width, int height) {
		Image img = new Image();
		img.setImage(image.getImage()
				.getSubimage(startX, startY, width, height));
		return img;
	}

	public static Image zoomImage(Image image, float zoomLevel) {
		int newImageWidth = (int) (image.getImage().getWidth() * zoomLevel);
		int newImageHeight = (int) (image.getImage().getHeight() * zoomLevel);
		BufferedImage resizedImage = new BufferedImage(newImageWidth,
				newImageHeight, java.awt.Image.SCALE_DEFAULT);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(image.getImage(), 0, 0, newImageWidth, newImageHeight, null);
		g.dispose();
		image.setImage(resizedImage);
		return image;
	}

	public static String readText(Image image) {
		ITesseract iTesseract = new Tesseract();
		try {
			String imgText = iTesseract.doOCR(image.getImage());
			return imgText;
		} catch (TesseractException e) {
			e.getMessage();
			return "Error while reading image";
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String readQROrBarCode(Image image) {

		try {
			Map hintMap = new HashMap();
			hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
			hintMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
			hintMap.put(DecodeHintType.POSSIBLE_FORMATS,
					EnumSet.allOf(BarcodeFormat.class));
			// hintMap.put(DecodeHintType.PURE_BARCODE, Boolean.FALSE);

			BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
					new BufferedImageLuminanceSource(image.getImage())));
			Result qrCodeResult;

			qrCodeResult = new MultiFormatReader()
					.decode(binaryBitmap, hintMap);
			return qrCodeResult.getText();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static int[] extractCoorditnatesFromQROrBarCode(Image image) {

		try {
			Map hintMap = new HashMap();
			hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

			BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
					new BufferedImageLuminanceSource(image.getImage())));
			ResultPoint[] qrCodeResult1 = new MultiFormatReader().decode(
					binaryBitmap).getResultPoints();
			ArrayList<Integer> x = new ArrayList<>();
			ArrayList<Integer> y = new ArrayList<>();
			System.out.println("qr code lenght " + qrCodeResult1.length);
			for (ResultPoint r : qrCodeResult1) {
				x.add((int) r.getX());
				y.add((int) r.getY());
				System.out.println(r.getX() + " " + r.getY());
			}
			Collections.sort(x, (l, r) -> {
				return Integer.compare(l, r);
			});
			Collections.sort(y, (l, r) -> {
				return Integer.compare(l, r);
			});
			int[] arr = { x.get(0), y.get(0), x.get(x.size() - 1) - x.get(0),
					y.get(y.size() - 1) - y.get(0) };
			return arr;
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void rotateImage90Degree(String inputFile, String outputFile,
			boolean clockwise) {
		try {
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

			Mat src = Imgcodecs.imread(inputFile);

			Mat dst = new Mat();
			/*if (clockwise) {
				Core.rotate(src, dst, Core.ROTATE_90_CLOCKWISE);
			} else {
				Core.rotate(src, dst, Core.ROTATE_90_COUNTERCLOCKWISE);
			}*/

			Imgcodecs.imwrite(outputFile, dst);

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public double compareImage(Image img, Image img2) {
		BufferedImage imgA = img.getImage();
		BufferedImage imgB = img2.getImage();
		int width1 = imgA.getWidth();
		int width2 = imgB.getWidth();
		int height1 = imgA.getHeight();
		int height2 = imgB.getHeight();
		long difference = 0;
		for (int y = 0; y < height1; y++) {
			for (int x = 0; x < width1; x++) {
				int rgbA = imgA.getRGB(x, y);
				int rgbB = imgB.getRGB(x, y);
				int redA = (rgbA >> 16) & 0xff;
				int greenA = (rgbA >> 8) & 0xff;
				int blueA = (rgbA) & 0xff;
				int redB = (rgbB >> 16) & 0xff;
				int greenB = (rgbB >> 8) & 0xff;
				int blueB = (rgbB) & 0xff;
				difference += Math.abs(redA - redB);
				difference += Math.abs(greenA - greenB);
				difference += Math.abs(blueA - blueB);
			}
		}
		double total_pixels = width1 * height1 * 3;
		double avg_different_pixels = difference / total_pixels;
		double percentage = (avg_different_pixels / 255) * 100;
		return percentage;

		
	}
}