package com.catalina.ImageComparator;


public interface Reader {
	public String read(Image image);
}
