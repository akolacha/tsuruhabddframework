package com.catalina.ImageComparator;

import java.io.IOException;


public interface Rotater {
	public Image rotate(Image img) throws IOException;
}
