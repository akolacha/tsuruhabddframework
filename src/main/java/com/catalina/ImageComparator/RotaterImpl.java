package com.catalina.ImageComparator;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;


public class RotaterImpl implements Rotater {

/*	public static void main(String[] args) {
		try {	
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
			Mat src = Imgcodecs
					.imread("D:/Workspace/Beta/test-output/Image/grayscale/Gm2.png");

			Mat dst = new Mat();
			Core.rotate(src, dst, Core.ROTATE_90_COUNTERCLOCKWISE);

			Imgcodecs.imwrite(
					"D:/Workspace/Beta/test-output/Image/rotated/Rm2.png", dst);

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
*/
	@Override
	public Image rotate(Image img) throws IOException {
		//System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		BufferedImage bufferedImage = img.getImage();

		Mat src = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(),
				CvType.CV_8UC3);
		byte[] data = ((DataBufferByte) bufferedImage.getRaster()
				.getDataBuffer()).getData();
		src.put(0, 0, data);

		Mat dst = new Mat();
		//Core.rotate(src, dst, Core.ROTATE_90_COUNTERCLOCKWISE);

		MatOfByte mob = new MatOfByte();
		// .jpg is not working 
		//Imgcodecs.imencode(".jpg", dst, mob);
		Imgcodecs.imencode(".bmp", dst, mob);
		byte ba[] = mob.toArray();

		BufferedImage bi = ImageIO.read(new ByteArrayInputStream(ba));

		img.setImage(bi);
		return img;
	}
}
