package com.catalina.ImageComparator;


import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;


public class TessractTextReader implements TextReader {

	@Override
	public String readText(Image image) {
		ITesseract iTesseract = new Tesseract();
		try {
			String imgText = iTesseract.doOCR(image.getImage());
			return imgText;
		} catch (TesseractException e) {
			e.getMessage();
			return "Error while reading image";
		}
	}

}
