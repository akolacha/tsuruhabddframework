package com.catalina.ImageComparator;


public interface TextReader {

	public String readText(Image image);
}
