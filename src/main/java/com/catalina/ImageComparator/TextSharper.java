package com.catalina.ImageComparator;


public interface TextSharper {

	public Image sharpeText(Image img);
}
