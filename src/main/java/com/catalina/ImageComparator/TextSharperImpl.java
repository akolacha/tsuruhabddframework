package com.catalina.ImageComparator;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;


public class TextSharperImpl implements TextSharper {

	@Override
	public Image sharpeText(Image img) {
		// TODO Auto-generated method stub
		BufferedImage bufferedImage = img.getImage();
		img.writeImage("abc","jpg","./report");
		Mat src = new Mat(bufferedImage.getHeight(), bufferedImage.getWidth(),
				CvType.CV_8UC1);
		byte[] data = ((DataBufferByte) bufferedImage.getRaster()
				.getDataBuffer()).getData();
		src.put(0, 0, data);

		Mat destination = new Mat(src.rows(), src.cols(), src.type());
		
//		Imgproc.GaussianBlur(src, src, new Size(0, 0), 10);
//		Imgproc.threshold(src, src, 0, 255, Imgproc.THRESH_OTSU);
		
		Imgproc.GaussianBlur(src, destination, new Size(0, 0), 10);
		Core.addWeighted(src, 1.5, destination, -0.5, 0, destination);

		
		// .jpg is not working
		// Imgcodecs.imencode(".jpg", dst, mob);
		MatOfByte mob = new MatOfByte();
		Imgcodecs.imencode(".bmp", destination, mob);
		
		
		byte ba[] = mob.toArray();

		BufferedImage bi = img.getImage();
		try {
			bi = ImageIO.read(new ByteArrayInputStream(ba));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		img.setImage(bi);
		img.writeImage("abc2","jpg","./report");
		return img;
	}

}
