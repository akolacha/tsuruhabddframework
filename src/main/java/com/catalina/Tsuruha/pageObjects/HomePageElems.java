package com.catalina.Tsuruha.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePageElems {

	WebDriver driver;

	public HomePageElems(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "show-landing-page")
	public WebElement landingPageCheckBox;

	@FindBy(xpath = "//*[@class='td528Button']")
	public WebElement proceedBtn;

	@FindBy(xpath = "//a[@href='/account/sign-in']")
	public WebElement LoginRegBtn;

	@FindBy(xpath = "//a[@href='/account/register']")
	public WebElement RegBtn;

	@FindBy(xpath = "//div[@class='list_btn clearfix']/div[2]/a[1]")
	public WebElement zipcode;

	@FindBy(id = "zip")
	public WebElement enterZipCodeTextArea;

	@FindBy(xpath = "//div[@class='show_block2 pt5 mb20 clearfix']//div[3]//a[1]")
	public WebElement prefectureIcon;

	@FindBy(xpath = "//div[@class='list-group list']//a[1]")
	public WebElement homeIcon;

	@FindBy(xpath = "//div[@id='main']//div[2]//div[1]//a[1]")
	public WebElement store;

	@FindBy(xpath = "//div[@class='lst_product lst_coupon clearfix list_view']")
	public WebElement couponsList;

	@FindBy(xpath = "//nav[@id='pointbar']//li[2]//a[1]")
	public WebElement basketPage;

	@FindBy(xpath = "//div[@class='list-group list']//a[1]")
	public WebElement myPageTopbtn;

	@FindBy(xpath = "//div[@class='owl-wrapper']/div[2]/div[1]/a[1]")
	public WebElement clipOfferStore;

	@FindBy(xpath = "//div[@class='lst_product lst_coupon clearfix list_view']")
	public WebElement couponListClipOffer;

	@FindBy(xpath = "//nav[@id='pointbar']/div[1]/ul[1]/li[2]/a[1]")
	public WebElement basketpage;

	@FindBy(xpath = "//div[@class='lst_product lst_coupon clearfix']/div[1]/a[1]/div[1]/p[1]")
	public WebElement clippedOfferBasketpage;

	@FindBy(linkText = "お店一覧")
	public WebElement storeListBtn;

	@FindBy(id = "MixItUp")
	public WebElement storeList;

	@FindBy(xpath = "//div[@id='main']/p[1]")
	public WebElement storebanner;

	@FindBy(xpath = "//p[@class='txt_pa04']")
	public WebElement landingPageMsg;

	@FindBy(xpath = "//div[@class='row']/h1/a")
	public WebElement logo;

	@FindBy(xpath = "//div[@class='page-title row']/h1")
	public WebElement myPage;

	@FindBy(xpath = "//div[@id='sub']/nav[1]")
	public WebElement topMenu;

	@FindBy(xpath = "//a[@href='/account']")
	public WebElement updateUserMenuItem;

	@FindBy(id = "newemail")
	public WebElement updateUserEmail;

	@FindBy(id = "newemailconfirm")
	public WebElement updateUserCnfEmail;

	@FindBy(id = "password")
	public WebElement updateUserPwd;

	@FindBy(id = "passwordconfirm")
	public WebElement updateUserCnfPwd;

	@FindBy(xpath = "//form[@id='userinfo_form']/fieldset/div[6]/button")
	public WebElement updateUserInfoBtn;

	@FindBy(xpath = "//div[@id='emailwrap']/small[1]")
	public WebElement updateUserEntervalidEmailMsg1;

	@FindBy(xpath = "//div[@id='emailwrap']/small[2]")
	public WebElement updateUserEntervalidEmailMsg2;

	@FindBy(xpath = "//div[@id='emailwrap']/small[3]")
	public WebElement updateUserEntervalidEmailMsg3;

	@FindBy(xpath = "//div[@id='passwordwrap']/small[1]")
	public WebElement updateUserEntervalidPWDMsg1;

	@FindBy(xpath = "//div[@id='passwordwrap']/small[3]")
	public WebElement updateUserEntervalidPWDMsg2;

	@FindBy(xpath = "//div[@id='main']/div[1]")
	public WebElement updateUserInfoSuccessMsg;
}
