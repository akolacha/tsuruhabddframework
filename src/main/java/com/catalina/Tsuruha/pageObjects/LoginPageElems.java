package com.catalina.Tsuruha.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPageElems {
	WebDriver driver;
	public LoginPageElems(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	@FindBy(id="email")
	public WebElement email;
	
	@FindBy(id="password")
	public WebElement password;
	
	@FindBy(xpath="//div[@class='form-group control-group text-center buttons noborder']/button")
	public WebElement loginBtn;
	
	@FindBy(xpath="//a[@href='/account/reset-password']")
	public WebElement forgotPassword;
	
	@FindBy(xpath="//input[@id='email']")
	public WebElement fpEnterEmail;
	
	@FindBy(xpath="//button[@class='btn btn_submit btn-lg btn-primary']")
	public WebElement resetPWBtn;
	
	@FindBy(id="doReset")
	public WebElement resetCnfBtn;
	
	@FindBy(xpath="//p[@class='password']")
	public WebElement resetPwdyopmail;
}
