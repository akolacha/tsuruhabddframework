package com.catalina.Tsuruha.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MiscPageElements {
	WebDriver driver;
	public MiscPageElements(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	@FindBy(id="login")
	public WebElement emailTxtField;
	
	@FindBy(className="sbut")
	public WebElement checkInboxBtn;
	
	@FindBy(css="#mailhaut")
	public WebElement RegistrationConf;
	
	@FindBy(id="password")
	public WebElement snappPWD;
	
	@FindBy(name="commit")
	public WebElement snappSignInBtn;
	
	@FindBy(xpath="//a[contains(text(),'Members')]")
	public WebElement snappMemebers;
	
	@FindBy(id="filter_email")
	public WebElement FilterEmail;
	
	@FindBy(xpath="//input[@value='Filter']")
	public WebElement FilterBtn;
	
	/*@FindBy(xpath="//table[@class='onable odded center']/tbody/tr[2]/td[3]")
	public WebElement snappEmailVerification;*/
	
	public WebElement snappEmailVerification(String email) {
		return driver.findElement(By.xpath("//td[contains(text(),'"+email+"')]"));
	}
	
	@FindBy(xpath="//a[@href='#delete_member']")
	public WebElement snappDeleteUser;
}
