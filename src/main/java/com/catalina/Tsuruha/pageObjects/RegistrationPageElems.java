package com.catalina.Tsuruha.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPageElems {
	WebDriver driver;
	public RegistrationPageElems(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	@FindBy(id="emailconfirm")
	public WebElement cnfEmail;
	
	@FindBy(id="passwordconfirm")
	public WebElement cnfPwd;
	
	@FindBy(id="checkboxes-1")
	public WebElement termsCheckbox;
	
	@FindBy(xpath="//*[text()='会員登録する']")
	public WebElement registerBtn;
	
}
