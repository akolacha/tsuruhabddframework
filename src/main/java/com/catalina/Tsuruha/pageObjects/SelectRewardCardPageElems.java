package com.catalina.Tsuruha.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SelectRewardCardPageElems {

	WebDriver driver;
	public SelectRewardCardPageElems(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
	@FindBy(xpath="//a[@href='/account/sign-out']")
	public WebElement logoutBtn;
	
}
