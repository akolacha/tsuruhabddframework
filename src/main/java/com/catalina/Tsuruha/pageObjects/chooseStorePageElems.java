package com.catalina.Tsuruha.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class chooseStorePageElems {

	WebDriver driver;
	public chooseStorePageElems(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
	@FindBy(xpath="//div[@class='list-group box_white_shadow lst_group_badge']/a[1]")
	public WebElement storelistsrore1;
	
	@FindBy(xpath="//h2[@class='section-title']")
	public WebElement selectStoreHeader;
	
	@FindBy(xpath="//div[@id='main']/a[5]")
	public WebElement selectState;
	
	@FindBy(xpath="//label[@class='checkbox']")
	public WebElement storeCheckBox;
	
	@FindBy(id="continue")
	public WebElement continueBtn;
	
	@FindBy(xpath = "//*[@id='main']/div[1]/a")
	public WebElement storename;
}
