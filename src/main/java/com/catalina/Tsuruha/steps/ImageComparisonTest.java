package com.catalina.Tsuruha.steps;

import com.catalina.ImageComparator.GrayScaleConvertorImpl;
import com.catalina.ImageComparator.HistogramCompareImage;
import com.catalina.ImageComparator.Image;

public class ImageComparisonTest {

/*@BeforeClass
public static  void beforeTest() {
	System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	System.out.println("--------------------------------------------------------");
}*/

	

public double testImage(String ExpImgName,String ActImgName) {
	System.out.println("Image Comaprision Started");
/*	Image img=new Image();
	img.readImage(ExpImgPath);
	GrayScaleConvertorImpl grayScaleConv=new GrayScaleConvertorImpl();
	img=	grayScaleConv.convert(img);
	Image img1=new Image();
	img1.readImage(ActImgPath);
	img1=	grayScaleConv.convert(img1);
	HistogramCompareImage comp=new HistogramCompareImage();
	comp.compareImage(img, img1);
	System.out.println("Image Comparision Complete");*/
	
	Image img = new Image();
	img.readImage(ExpImgName, "png",
			"./Expected_Screenshots\\");
	GrayScaleConvertorImpl grayScaleConv = new GrayScaleConvertorImpl();
	img = grayScaleConv.convert(img);
	Image img2 = new Image();
	img2.readImage(ActImgName,"png","./target\\cucumber-reports\\screenshots\\");
	img2 = grayScaleConv.convert(img2);
	HistogramCompareImage comp = new HistogramCompareImage();
	return comp.compareImage(img, img2);
}
}
	

