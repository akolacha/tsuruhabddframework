package com.catalina.Tsuruha.steps;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.Tsuruha.pageObjects.HomePageElems;
import com.catalina.Tsuruha.pageObjects.LoginPageElems;
import com.catalina.Tsuruha.pageObjects.MiscPageElements;
import com.catalina.Tsuruha.pageObjects.RegistrationPageElems;
import com.catalina.Tsuruha.pageObjects.SelectRewardCardPageElems;
import com.catalina.Tsuruha.utils.ReusableMethods;
import com.catalina.Tsuruha.utils.SharedResources;

public class clipOffers {
	WebDriver driver;
	WebDriverWait wait;
	HomePageElems hp;
	LoginPageElems lp;
	SelectRewardCardPageElems rp;
	RegistrationPageElems regp;
	ReusableMethods reuseMethods;
	String email;
	String pwd;
	MiscPageElements mp;
	SharedResources sharedResource;
	String couponName = "";
	JavascriptExecutor js;

	//private static Logger logger = Logger.getLogger(clipOffers.class);

	public clipOffers(SharedResources sharedResource) {
		this.driver = sharedResource.init();
		wait = new WebDriverWait(driver, 300);
		hp = new HomePageElems(driver);
		lp = new LoginPageElems(driver);
		rp = new SelectRewardCardPageElems(driver);
		regp = new RegistrationPageElems(driver);
		mp = new MiscPageElements(driver);
		js = (JavascriptExecutor) driver;

	}

	/*@Then("^Navigate to coupons page and clip an offer$")
	public void navigate_to_coupons_page_and_clip_an_offer() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(hp.myPageTopbtn));
		hp.myPageTopbtn.click();
		logger.info("My page top button clicked");
		wait.until(ExpectedConditions.visibilityOf(hp.storeListBtn));
		hp.storeListBtn.click();
		logger.info("Clicked on the store list button");
		wait.until(ExpectedConditions.visibilityOf(hp.storeList));
		List<WebElement> stores = driver.findElements(By.xpath("//a[@class='detail_link']"));
		// System.out.println(stores);
		outerloop: for (int j = 0; j < stores.size(); j++) {
			wait.until(ExpectedConditions.visibilityOf(stores.get(j)));
			Actions action = new Actions(driver);
			action.keyDown(Keys.CONTROL).build().perform();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", stores.get(j));
			stores.get(j).click();
			action.keyUp(Keys.CONTROL).build().perform();
			String defaultwindow = driver.getWindowHandle();
			for (String handle1 : driver.getWindowHandles()) {
				System.out.println(handle1);
				driver.switchTo().window(handle1);
			}
			logger.info("Store clicked");
			String banner = "合計0クーポン 0pお得!!";
			wait.until(ExpectedConditions.visibilityOf(hp.storebanner));
			innerloop: if (hp.storebanner.getText().equals(banner)) {
				driver.close();
				driver.switchTo().window(defaultwindow);
				logger.info("No offers present");
				break innerloop;
			} else {
				wait.until(ExpectedConditions.visibilityOf(hp.couponListClipOffer));
				List<WebElement> offers = hp.couponListClipOffer.findElements(By.tagName("div"));
				for (int i = 0; i < offers.size(); i++) {
					try {
						WebElement elem1 = offers.get(i).findElement(By.xpath("//div[@class='product_detail']/a[2]"));
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elem1);
						elem1.click();
						logger.info("offer clipped");
						couponName = offers.get(i).findElement(By.xpath("//div[@class='product_detail']/a[1]/p[1]"))
								.getText().substring(0, 14);
						logger.info("Coupon name stored:" + couponName);
						break outerloop;
					} catch (Exception e) {
						logger.info("Clip button not available" + e);
					}
				}
			}

		}
	}

	@Then("^navigate to basket page and verify that offer is clipped$")
	public void navigate_to_basket_page_and_verify_that_offer_is_clipped() {
		wait.until(ExpectedConditions.visibilityOf(hp.basketpage));
		hp.basketPage.click();
		wait.until(ExpectedConditions.visibilityOf(hp.clippedOfferBasketpage));
		String s = hp.clippedOfferBasketpage.getText().substring(0, 14);
		logger.info("Verified Offer " + s);
		assertEquals(couponName, s);
	}*/
}
