package com.catalina.Tsuruha.steps;


import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.Tsuruha.pageObjects.HomePageElems;
import com.catalina.Tsuruha.pageObjects.LoginPageElems;
import com.catalina.Tsuruha.pageObjects.MiscPageElements;
import com.catalina.Tsuruha.pageObjects.RegistrationPageElems;
import com.catalina.Tsuruha.pageObjects.SelectRewardCardPageElems;
import com.catalina.Tsuruha.utils.ReusableMethods;
import com.catalina.Tsuruha.utils.ScreenShot;
import com.catalina.Tsuruha.utils.SharedResources;
import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/****
 * @author M1054882 Soumalya
 ***/

public class loginLogout_tsuruha {
	WebDriver driver;
	WebDriverWait wait;
	HomePageElems hp;
	LoginPageElems lp;
	SelectRewardCardPageElems rp;
	RegistrationPageElems regp;
	ReusableMethods reuseMethods;
	String email;
	String pwd;
	MiscPageElements mp;
	SharedResources sharedResource;
	String pageTitle;
	String resetPWEmail;
	String newPWD;
	String couponName = "";
	ImageComparisonTest imgComp;
	int flag;
	private static Logger logger = Logger.getLogger(loginLogout_tsuruha.class);

	public loginLogout_tsuruha(SharedResources sharedResource) {
		this.driver = sharedResource.init();
		wait = new WebDriverWait(driver, 150);
		hp = new HomePageElems(driver);
		lp = new LoginPageElems(driver);
		rp = new SelectRewardCardPageElems(driver);
		regp = new RegistrationPageElems(driver);
		mp = new MiscPageElements(driver);
		imgComp=new ImageComparisonTest();

	}

	@Given("^Navigate to Tsuruha \"([^\"]*)\"$")
	public void navigate_to_Tsuruha(String arg1) {
		driver.get(arg1);
		logger.info(arg1 + " URL entered in Browser");
	}

	@Given("^Logout as user is logged in$")
	public void Logout_as_user_is_logged_in() {
		String pageTitle = driver.getTitle();
		if (pageTitle.equals("お店を検索") || pageTitle.equals("登録")) {
			wait.until(ExpectedConditions.visibilityOf(rp.logoutBtn));
			rp.logoutBtn.click();
			logger.info("logout button clicked");
			wait.until(ExpectedConditions.visibilityOf(hp.LoginRegBtn));
			hp.LoginRegBtn.click();
			logger.info("login Register button clicked");
		}
	}

	@When("^Navigate to Catalina Page$")
	public void landing_Page_Displayed() {
		try {
			wait.until(ExpectedConditions.visibilityOf(hp.landingPageCheckBox));
			logger.info("Landing page displayed");
			hp.landingPageCheckBox.click();
			hp.proceedBtn.click();
			logger.info("Landing page check box and Procced button clicked");
		} catch (Exception e) {
			logger.info(e);
		}
	}
	
	@Then("^Enter credentials and click on the Login Button$")
	public void enter_credentials_and_click_on_the_Login_Button() throws InterruptedException, IOException {
		
		String arg1=reuseMethods.getData("email");
		String arg2=reuseMethods.getData("password");
		wait.until(ExpectedConditions.visibilityOf(lp.email));
		lp.email.clear();
		Thread.sleep(1000);
		lp.email.sendKeys(arg1);
		lp.password.clear();
		Thread.sleep(1000);
		lp.password.sendKeys(arg2 + Keys.TAB);
		Thread.sleep(1000);
		logger.info(arg1 + "::" + arg2 + "  EmailId,Password entered");
		if (lp.loginBtn.isEnabled()) {
			wait.until(ExpectedConditions.elementToBeClickable(lp.loginBtn));
			lp.loginBtn.click();
			logger.info("Login button clicked");
			Thread.sleep(1000);
			
		}
	}
		

	@Then("^navigate to login page$")
	public void click_on_Login_button() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(hp.LoginRegBtn));
		Thread.sleep(500);
		hp.LoginRegBtn.click();
		logger.info("LoginRegBtn button clicked");
	}

	@Then("^Enter  \"([^\"]*)\" and  \"([^\"]*)\" and click on the Login Button$")
	public void enter_valid_and_Valid(String arg1, String arg2) throws InterruptedException, IOException {
		// String s1="ポイントカードを選択";
		wait.until(ExpectedConditions.visibilityOf(lp.email));
		lp.email.clear();
		Thread.sleep(1000);
		lp.email.sendKeys(arg1);
		lp.password.clear();
		Thread.sleep(1000);
		lp.password.sendKeys(arg2 + Keys.TAB);
		Thread.sleep(1000);
		logger.info(arg1 + "::" + arg2 + "  EmailId,Password entered");
		if (lp.loginBtn.isEnabled()) {
			wait.until(ExpectedConditions.elementToBeClickable(lp.loginBtn));
			lp.loginBtn.click();
			Thread.sleep(1000);
			
			//ScreenShot.addScreenshotToScreen(driver,"AfterLoginPage",0);
			/*
			 * if(!arg2.equals("test321")) {
			 * wait.until(ExpectedConditions.visibilityOf(hp.myPage)); Assert.assertEquals(s1,
			 * hp.myPage.getText()); }
			 */
			//ImageProcessingControllerWithMain.compare();
			logger.info("Login button clicked");
		}
	}

	@Then("^navigate to Registration page$")
	public void navigate_to_Registration_page() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(hp.RegBtn));
			Thread.sleep(500);
			hp.RegBtn.click();
			logger.info("registration button clicked");
		} catch (Exception e) {
			logger.info(e);
		}
	}

	@Then("^Enter email and password and click on membership button$")
	public void enter_email_and_password_and_click_on_membership_button() throws InterruptedException {
		email = ReusableMethods.randomEmail();
		pwd = "test123";
		wait.until(ExpectedConditions.visibilityOf(lp.email));
		lp.email.sendKeys(email);
		regp.cnfEmail.sendKeys(email);
		lp.password.sendKeys(pwd);
		regp.cnfPwd.sendKeys(pwd);
		regp.termsCheckbox.click();
		// wait.until(ExpectedConditions.elementToBeClickable(regp.registerBtn));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", regp.registerBtn);
		regp.registerBtn.click();
		logger.info("Email and password entered and register button clicked");
		Thread.sleep(2000);

	}

	@Then("^Verify registration Successful$")
	public void verify_registration_Successful() throws InterruptedException {
		driver.get(ReusableMethods.getData("SnappJPUrl"));
		logger.info("Snapp url entered");
		wait.until(ExpectedConditions.visibilityOf(mp.emailTxtField));
		mp.emailTxtField.sendKeys(ReusableMethods.getData("snappUserID"));
		mp.snappPWD.sendKeys(ReusableMethods.getData("snappPWD"));
		mp.snappSignInBtn.click();
		logger.info("email entered and check inbox button clicked");
		wait.until(ExpectedConditions.visibilityOf(mp.snappMemebers));
		mp.snappMemebers.click();
		wait.until(ExpectedConditions.visibilityOf(mp.FilterEmail));
		mp.FilterEmail.sendKeys(email);
		mp.FilterBtn.click();
		logger.info("Registration Successful");
		mp.snappDeleteUser.click();
		driver.switchTo().alert().accept();
		logger.info("User deleted from snapp");
	}

	@Then("^Navigate to yopmail \"([^\"]*)\" and verify$")
	public void navigate_to_yopmail(String arg1) throws InterruptedException, IOException {
		driver.get(arg1);
		logger.info("yopmail url entered");
		wait.until(ExpectedConditions.visibilityOf(mp.emailTxtField));
		mp.emailTxtField.sendKeys(resetPWEmail);
		mp.checkInboxBtn.click();
		logger.info("email entered and check inbox button clicked");
		driver.switchTo().frame("ifmail");
		newPWD = lp.resetPwdyopmail.getText();
		driver.get("https://tsuruha-uat.couponnetwork.jp/");
		click_on_Login_button();
		enter_valid_and_Valid(resetPWEmail, newPWD);
	}

	@Then("^Enter credential and verify login successful$")
	public void enter_credential_and_verify_login_successful() throws InterruptedException, IOException {
		enter_valid_and_Valid(email, pwd);
	}

	@Then("^Verify the Welcome Page$")
	public void verify_the_Welcome_Page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOf(hp.landingPageMsg));
		String welcomeMsg = "ポイント還元型サービス「カタリナ」へようこそ！";
		Assert.assertEquals(welcomeMsg, hp.landingPageMsg.getText());
		logger.info("welcome message verified");
		hp.LoginRegBtn.click();
		/*ScreenShot.addScreenshotToScreen(driver,"AfterLoginPage",0);
		double d=imgComp.testImage("LoggedInPage", "AfterLoginPage");
		//System.out.println(d);
		Assert.assertTrue(d>=100);
		logger.info("image compared");*/
	}

	@Then("^Verify Select Reward Page is displayed$")
	public void verify_Select_Reward_Page_is_displayed() {
		pageTitle = "お店を検索";
		Assert.assertEquals(pageTitle, driver.getTitle());
	}

	@Then("^Click on Site Logo and verify \"([^\"]*)\" is displayed$")
	public void click_on_Site_Logo_and_verify_is_displayed(String arg1) {
		wait.until(ExpectedConditions.visibilityOf(hp.logo));
		hp.logo.click();
		logger.info("Page Logo Clicked");
		pageTitle = "ようこそカタリナへ";
		if (arg1.equals("Same Page")) {
			Assert.assertEquals(pageTitle, driver.getTitle());
			hp.LoginRegBtn.click();
		} else if (arg1.equals("My Page")) {
			pageTitle = "マイページ";
			wait.until(ExpectedConditions.visibilityOf(hp.myPage));
			Assert.assertEquals(pageTitle, hp.myPage.getText());
		}
		logger.info(arg1 + " page verified");
	}

	@Then("^Verify the menu items for \"([^\"]*)\"$")
	public void verify_the_menu_items_for(String arg1) throws InterruptedException, IOException {
		reuseMethods=new ReusableMethods();
		List<WebElement> menus = hp.topMenu.findElements(By.tagName("a"));
		List<String> expMenuItem = new LinkedList<>();
		if (arg1.equals("Anonymous User")) {
			expMenuItem.add("会員登録はこちら");
			expMenuItem.add("お店を検索");
			for (int i = 0; i < menus.size(); i++) {
				Assert.assertEquals(expMenuItem.get(i), menus.get(i).getText());
				logger.info("Expected" + expMenuItem.get(i) + "Actual " + menus.get(i).getText());
			}
			if(reuseMethods.getBrowserName().equalsIgnoreCase("Chrome")){
			//Comapairing the menu items with stored image
			ScreenShot.addScreenshotToScreen(driver,"ActBeforeLogin_Chrome",0);
			double d=imgComp.testImage("ExpBeforeLogin_Chrome", "ActBeforeLogin_Chrome");
			System.out.println(d);
			Assert.assertTrue(d>=100);
			logger.info("image compared");
			}
			else {
				ScreenShot.addScreenshotToScreen(driver,"ActBeforeLogin_Firefox",0);
				double d=imgComp.testImage("ExpBeforeLogin_Firefox", "ActBeforeLogin_Firefox");
				//System.out.println(d);
				Assert.assertTrue(d>=100);
				logger.info("image compared");
			}
		} else if (arg1.equals("Logged In User")) {
			//Comapairing the menu items with stored image
			if(reuseMethods.getBrowserName().equalsIgnoreCase("Chrome")){
			ScreenShot.addScreenshotToScreen(driver,"ActLoggedInPage_Chrome",0);
			double d=imgComp.testImage("ExpLoggedInPage_Chrome", "ActLoggedInPage_Chrome");
			//System.out.println(d);
			Assert.assertTrue(d>=100);
			logger.info("image compared");
			}
			else {
					ScreenShot.addScreenshotToScreen(driver,"ActAfterLoginPage_Firefox",0);
					double d=imgComp.testImage("ExpLoggedInPage_Firefox", "ActAfterLoginPage_Firefox");
					//System.out.println(d);
					Assert.assertTrue(d>=100);
					logger.info("image compared");
			}
			expMenuItem.add("マイページトップ");
			expMenuItem.add("マイカード");
			expMenuItem.add("ユーザー情報変更");
			expMenuItem.add("カタリナって？");
			expMenuItem.add("お店を検索");
			for (int i = 0; i < menus.size(); i++) {
				Assert.assertEquals(expMenuItem.get(i), menus.get(i).getText());
				logger.info("Expected" + expMenuItem.get(i) + "Actual " + menus.get(i).getText());
			}
		}
	}

	@Then("^Navigate to login page and click on forgot password link$")
	public void navigate_to_login_page_and_click_on_forgot_password_link() {
		wait.until(ExpectedConditions.visibilityOf(hp.LoginRegBtn));
		hp.LoginRegBtn.click();
		logger.info("Login register button cliked");
		wait.until(ExpectedConditions.visibilityOf(lp.forgotPassword));
		lp.forgotPassword.click();
		logger.info("forgot password link clicked");

	}

	@Then("^Enter \"([^\"]*)\" and reset password$")
	public void enter_and_reset_password(String arg1) {
		resetPWEmail = arg1;
		wait.until(ExpectedConditions.visibilityOf(lp.fpEnterEmail));
		lp.fpEnterEmail.sendKeys(Keys.chord(resetPWEmail + Keys.TAB));
		lp.resetPWBtn.click();
		logger.info("lp.resetPWBtn button clicked");
		wait.until(ExpectedConditions.visibilityOf(lp.resetCnfBtn));
		lp.resetCnfBtn.click();
		logger.info("lp.resetCnfBtn button clicked");
	}

	@Then("^Enter different combinations in the email field and verify$")
	public void enter_different_combinations_in_the_email_field_and_verify() {
		if (flag != 1) {
			wait.until(ExpectedConditions.visibilityOf(hp.updateUserMenuItem));
			hp.updateUserMenuItem.click();
			logger.info("Update user info menu item clicked");
			wait.until(ExpectedConditions.visibilityOf(hp.updateUserEmail));
			// Invalid Email
			hp.updateUserEmail.sendKeys(Keys.chord("INVALID_EMAIL" + Keys.TAB + "INVALID_EMAIL" + Keys.TAB));
			String msg = "正しいメールアドレスを入力してください";
			Assert.assertEquals(msg, hp.updateUserEntervalidEmailMsg1.getText());
			Assert.assertEquals(msg, hp.updateUserEntervalidEmailMsg2.getText());
			Reporter.addStepLog("Invalid Email Entered and Validation Passed");

			// Different emails entered in the email fields
			hp.updateUserEmail.clear();
			hp.updateUserEmail.sendKeys(Keys.chord("test@yopmail.com" + Keys.TAB));
			hp.updateUserCnfEmail.sendKeys(Keys.chord("test1@yopmail.com" + Keys.TAB));
			msg = "メールアドレスが一致しません。";
			Assert.assertEquals(msg, hp.updateUserEntervalidEmailMsg3.getText());
			Reporter.addStepLog("Different email entered in confirm email address and Validation Passed");
		}
	}

	@Then("^Enter different combination in the password field and verify$")
	public void enter_different_combination_in_the_password_field_and_verify() {
		if (flag != 1) {
			// validating with 4 characters
			hp.updateUserPwd.sendKeys(Keys.chord("asdf" + Keys.TAB + "asdf" + Keys.TAB));
			String msg = "パスワードは6文字以上で入力してください";
			Assert.assertEquals(msg, hp.updateUserEntervalidPWDMsg1.getText());
			logger.info("4 characters entered in both the password box");
			Reporter.addStepLog("Invalid password Entered and Validation Passed");
			// Entering different Strings in the password fields
			hp.updateUserPwd.sendKeys(Keys.chord("test456" + Keys.TAB));
			hp.updateUserCnfPwd.sendKeys("test123" + Keys.TAB);
			logger.info("differeent passsword entered");
			msg = "パスワードが一致しません";
			Assert.assertEquals(msg, hp.updateUserEntervalidPWDMsg2.getText());
			logger.info("Validation passed with different password");
			Reporter.addStepLog("Different password entered in confirm password address and Validation Passed");

		}
	}

	@Then("^Enter \"([^\"]*)\" and \"([^\"]*)\" and update$")
	public void enter_and_and_update(String arg1, String arg2) throws InterruptedException {
		if (arg2.equals("test321")) {
			flag = 1;
		}
		hp.updateUserEmail.clear();
		hp.updateUserEmail.sendKeys(arg1);
		hp.updateUserCnfEmail.clear();
		hp.updateUserCnfEmail.sendKeys(Keys.chord(arg1 + Keys.TAB));
		hp.updateUserPwd.clear();
		hp.updateUserPwd.sendKeys(arg2);
		hp.updateUserCnfPwd.clear();
		hp.updateUserCnfPwd.sendKeys(Keys.chord(arg2 + Keys.TAB));
		wait.until(ExpectedConditions.visibilityOf(hp.updateUserInfoBtn));
		hp.updateUserInfoBtn.click();
		// String confmsg="アカウントが正常に更新されました".trim();
		Thread.sleep(2000);
		Assert.assertTrue(hp.updateUserInfoSuccessMsg.isDisplayed());
		logger.info("Entered emails and passwords");
	}

	@Then("^Navigate to coupons page and clip an offer$")
	public void navigate_to_coupons_page_and_clip_an_offer() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(hp.myPageTopbtn));
		hp.myPageTopbtn.click();
		logger.info("My page top button clicked");
		wait.until(ExpectedConditions.visibilityOf(hp.storeListBtn));
		hp.storeListBtn.click();
		logger.info("Clicked on the store list button");
		wait.until(ExpectedConditions.visibilityOf(hp.storeList));
		List<WebElement> stores = driver.findElements(By.xpath("//a[@class='detail_link']"));
		// System.out.println(stores);
		outerloop: for (int j = 0; j < stores.size(); j++) {
			wait.until(ExpectedConditions.visibilityOf(stores.get(j)));
			Actions action = new Actions(driver);
			action.keyDown(Keys.CONTROL).build().perform();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", stores.get(j));
			stores.get(j).click();
			action.keyUp(Keys.CONTROL).build().perform();
			String defaultwindow = driver.getWindowHandle();
			for (String handle1 : driver.getWindowHandles()) {
				System.out.println(handle1);
				driver.switchTo().window(handle1);
			}
			logger.info("Store clicked");
			String banner = "合計0クーポン 0pお得!!";
			wait.until(ExpectedConditions.visibilityOf(hp.storebanner));
			innerloop: if (hp.storebanner.getText().equals(banner)) {
				driver.close();
				driver.switchTo().window(defaultwindow);
				logger.info("No offers present");
				break innerloop;
			} else {
				wait.until(ExpectedConditions.visibilityOf(hp.couponListClipOffer));
				List<WebElement> offers = hp.couponListClipOffer.findElements(By.tagName("div"));
				for (int i = 0; i < offers.size(); i++) {
					try {
						WebElement elem1 = offers.get(i).findElement(By.xpath("//div[@class='product_detail']/a[2]"));
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elem1);
						// *********This will clip the offer**********
						elem1.click();
						logger.info("offer clipped");
						couponName = offers.get(i).findElement(By.xpath("//div[@class='product_detail']/a[1]/p[1]"))
								.getText().substring(0, 14);
						logger.info("Coupon name stored:" + couponName);
						break outerloop;
					} catch (Exception e) {
						logger.info("Clip button not available" + e);
					}
				}
			}

		}
	}

	@Then("^navigate to basket page and verify that offer is clipped$")
	public void navigate_to_basket_page_and_verify_that_offer_is_clipped() {
		wait.until(ExpectedConditions.visibilityOf(hp.basketpage));
		hp.basketPage.click();
		wait.until(ExpectedConditions.visibilityOf(hp.clippedOfferBasketpage));
		String s = hp.clippedOfferBasketpage.getText().substring(0, 14);
		logger.info("Verified Offer " + s);
		Assert.assertEquals(couponName, s);
	}

}
