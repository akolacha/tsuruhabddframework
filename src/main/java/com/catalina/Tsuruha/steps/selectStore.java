package com.catalina.Tsuruha.steps;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.Tsuruha.pageObjects.HomePageElems;
import com.catalina.Tsuruha.pageObjects.LoginPageElems;
import com.catalina.Tsuruha.pageObjects.MiscPageElements;
import com.catalina.Tsuruha.pageObjects.RegistrationPageElems;
import com.catalina.Tsuruha.pageObjects.SelectRewardCardPageElems;
import com.catalina.Tsuruha.pageObjects.chooseStorePageElems;
import com.catalina.Tsuruha.utils.ReusableMethods;
import com.catalina.Tsuruha.utils.SharedResources;

import cucumber.api.java.en.Then;

/****
 * @author M1054882 Soumalya
 ***/
public class selectStore {
	WebDriver driver;
	WebDriverWait wait;
	HomePageElems hp;
	LoginPageElems lp;
	SelectRewardCardPageElems rp;
	RegistrationPageElems regp;
	ReusableMethods reuseMethods;
	String email;
	String pwd;
	MiscPageElements mp;
	SharedResources sharedResource;
	chooseStorePageElems sp;
	int flag = 0;

	private static Logger logger = Logger.getLogger(selectStore.class);

	public selectStore(SharedResources sharedResource) {
		this.driver = sharedResource.init();
		wait = new WebDriverWait(driver, 300);
		hp = new HomePageElems(driver);
		lp = new LoginPageElems(driver);
		rp = new SelectRewardCardPageElems(driver);
		regp = new RegistrationPageElems(driver);
		mp = new MiscPageElements(driver);
		sp = new chooseStorePageElems(driver);
	}

	@Then("^Click on the zipcode icon$")
	public void click_on_the_zipcode_icon() {
		wait.until(ExpectedConditions.visibilityOf(hp.zipcode));
		hp.zipcode.click();
		Assert.assertTrue(driver.getTitle().equals("郵便番号で探す：「カタリナクーポンがいらないクーポン"));
		logger.info("zipcode icon clicked");
	}

	@Then("^Enter zipcode \"([^\"]*)\" and search$")
	public void enter_zipcode_and_search(String arg1) {
		// wait.until(ExpectedConditions.visibilityOf(hp.enterZipCodeTextArea));
		hp.enterZipCodeTextArea.sendKeys(arg1);
		hp.enterZipCodeTextArea.sendKeys(Keys.chord(Keys.TAB, Keys.ENTER));
		logger.info("Zipcodee entered");
	}

	@Then("^Select a store and navigate to the store$")
	public void select_a_store_and_navigate_to_the_store() throws IOException, InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(sp.selectStoreHeader));
		// Assert.assertTrue(sp.storelistsrore1.isDisplayed());
		if (flag == 1) {
			/*
			 * wait.until(ExpectedConditions.visibilityOf(sp.selectState));
			 * sp.selectState.click();
			 */
			logger.info("Navigating to store");
			for (int i = 0; i < 4; i++) {
				// wait.until(ExpectedConditions.visibilityOf(sp.storelistsrore1));
				sp.storelistsrore1.click();
			}
			wait.until(ExpectedConditions.visibilityOf(sp.storeCheckBox));
			sp.storeCheckBox.click();
			sp.continueBtn.click();

			Assert.assertTrue(sp.storename.isDisplayed());

		} else {
			sp.storelistsrore1.click();
		}
	}

	@Then("^Click on the prefecture icon$")
	public void click_on_the_prefecture_icon() {
		wait.until(ExpectedConditions.visibilityOf(hp.prefectureIcon));
		hp.prefectureIcon.click();
		Assert.assertTrue(driver.getTitle().equals("都道府県から探す"));
		flag = 1;
	}

}
