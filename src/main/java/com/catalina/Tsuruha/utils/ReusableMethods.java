package com.catalina.Tsuruha.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Properties;

public class ReusableMethods {

	public static String getData(String data) {
		Properties prop = new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream("./config.properties");
			prop.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String s = prop.getProperty(data);
		return s;
	}

	public static String randomEmail() {
		SecureRandom random = new SecureRandom();
		String randStr = new BigInteger(32, random).toString(16);
		String email = randStr + getData("RegistrationEmail");
		return email;
	}

	public int getBuildID() {
		String build = new String();
		build = getData("BuildId");
		return Integer.parseInt(build);
	}

	public String getBuildName() {
		System.out.println("Inside getBuildName");

		if (System.getProperty("BuildName") == null) {
			return getData("BuildName");
		} else {

			return System.getProperty("BuildName");
		}

	}

	public String getTestPlanID() {
		if (System.getProperty("TestPlanID") == null) {
			return getData("TestPlanId");
		} else {

			return System.getProperty("TestPlanID");
		}
	}

	public String getBrowserName() {
		if (System.getProperty("browserName") == null) {
			return getData("browser");
		} else {
			return System.getProperty("browserName");
		}
	}
}
