package com.catalina.Tsuruha.utils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.cucumber.listener.Reporter;
import com.google.common.io.Files;

public class ScreenShot {
	//If flag=1 it will attach the screenshot to the report.
	public static void addScreenshotToScreen(WebDriver driver, String imageName,int flag)
			throws InterruptedException, IOException {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		File destination = new File(System.getProperty("user.dir") + "/target/cucumber-reports/screenshots");

		if (!destination.exists()) {
			System.out.println("File created " + destination);
			destination.mkdir();
		}

		File destinationPath = new File(
				System.getProperty("user.dir") + "/target/cucumber-reports/screenshots/" + imageName + ".png");

		Files.copy(sourcePath, destinationPath);

		if(flag==1) {
		Reporter.addStepLog("<img src=\"./screenshots/" + imageName + ".png" + "\" style=\"height:600px;width:100%\">");
		}
		}
}
