package com.catalina.Tsuruha.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import entities.TestCaseRun;
import runner.TestRunner;

public class SharedResources {
	protected static WebDriver driver;
	Scenario scenario;
	TestRunner testRunner;
	ReusableMethods reusableMethods;
	public WebDriver init() {
		if (driver == null) {
//			if (ReusableMethods.getData("browser").equalsIgnoreCase("Chrome")) {
//				WebDriverManager.chromedriver().setup();
//				DesiredCapabilities cap = DesiredCapabilities.chrome();
//				cap.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
//				driver = new ChromeDriver(cap);
//				driver.manage().window().maximize();
//			} else if (ReusableMethods.getData("browser").equalsIgnoreCase("Firefox")) {
//				WebDriverManager.firefoxdriver().setup();
//				DesiredCapabilities cap = DesiredCapabilities.firefox();
//				cap.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
//				driver = new FirefoxDriver(cap);
//				driver.manage().window().maximize();
			reusableMethods = new ReusableMethods();
			if(reusableMethods.getBrowserName().equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.driver.chrome", "./chromedriver.exe");
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
			driver = new ChromeDriver(cap);
			driver.manage().window().maximize();
			}
			else if(reusableMethods.getBrowserName().equalsIgnoreCase("Firefox")) {
				System.setProperty("webdriver.driver.firefox", "./geckodriver.exe");
				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
				driver = new FirefoxDriver(cap);
				driver.manage().window().maximize();
			}
		}
		return driver;
	}

	@After()
	public void afterHook(Scenario scenario) throws Throwable {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String time = sdf.format(timestamp);
		System.out.println("Completed the Scenario--" + scenario.getName());
		String name = scenario.getName();
		if (name.contains("TestCaseIds")) {
			String[] list = name.split("TestCaseIds");
			String[] list1 = list[1].split(",");
			int[] testcaseId = Arrays.asList(list1).stream().map(String::trim).mapToInt(Integer::parseInt).toArray();
			TargetProcess tp = new TargetProcess();
			try {

				String status = scenario.getStatus();
				System.out.println("Status  " + status);
				TestCaseRun testCaseRun = new TestCaseRun(status, "Automation Update");
				for (int i = 0; i < testcaseId.length; i++) {
					if (testRunner.buildID != -1) {
						tp.updateStatus(testRunner.buildID, testcaseId[i], testCaseRun);
					}
				}
				if(status!="passed") {
					ScreenShot.addScreenshotToScreen(driver, time,1);
					}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Exception : In Method: UpdateTCResult() " + e.getMessage());
			}
		} else {
			System.out.println("No id's Present");
		}
		driver.quit();
		driver = null;
	}

}
