package com.catalina.Tsuruha.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class log4jConfiguration {
	private static Logger logger = Logger.getLogger(log4jConfiguration.class);
	public log4jConfiguration() throws FileNotFoundException, IOException {
		//PropertiesConfigurator is used to configure logger from properties file
		Properties props = new Properties();
		props.load(new FileInputStream("./src/main/java/log4j.properties"));
	      PropertyConfigurator.configure(props);
	}
	/*public static void log(String message) {
		//PropertiesConfigurator is used to configure logger from properties file
       // PropertyConfigurator.configure("log4j.Properties");
        logger.debug("Sample debug message");
		logger.info("Sample info message");
		logger.warn("Sample warn message");
		logger.error("Sample error message");
		logger.fatal("Sample fatal message");
 
        //Log in console in and log file
        logger.debug(message);
	}*/
	public void testLoggerDebug(String message) {
		logger.debug(message);
	}

	public void testLoggerInfo() {
		logger.info("Hello.. im in Info method");
	}

	public void testLoggerWarn() {
		logger.warn("Hello.. im in Warn method");
	}

	public void testLoggerError() {
		logger.error("Hello.. im in Error method");
	}

	public void testLoggerFatal() {
		logger.fatal("Hello.. im in Fatal method");
	}
	
}
