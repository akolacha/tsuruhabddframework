package runner;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opencv.core.Core;

import com.catalina.ImageComparator.GrayScaleConvertorImpl;
import com.catalina.ImageComparator.HistogramCompareImage;
import com.catalina.ImageComparator.Image;

public class ImageComparisonTest {

	@BeforeClass
	public static void beforeTest() {

		System.out
				.println("-----------------------------------------------------------------------");
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	@Test
	public void testImage() {
		System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		Image img = new Image();
		img.readImage("LoggedInPage", "png",
				"./Expected_Screenshots\\");
		GrayScaleConvertorImpl grayScaleConv = new GrayScaleConvertorImpl();
		Image img1 = grayScaleConv.convert(img);
		img.readImage(
				"AfterLoginPage",
				"png",
				"./target\\cucumber-reports\\screenshots\\");
		Image img2 = grayScaleConv.convert(img);
		HistogramCompareImage comp = new HistogramCompareImage();
		comp.compareImage(img1, img2);
	}
}
