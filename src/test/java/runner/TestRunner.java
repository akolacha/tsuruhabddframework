package runner;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.opencv.core.Core;

import com.catalina.Tsuruha.utils.ReusableMethods;
import com.catalina.Tsuruha.utils.TargetProcess;
import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "Features/loginLogout.feature", "Features/registration.feature",
		"Features/selectStore.feature", "Features/clipOffer.feature" }, glue = { "com.catalina.Tsuruha.steps",
				"com.catalina.Tsuruha.utils" }, dryRun = false, plugin = {
						"com.cucumber.listener.ExtentCucumberFormatter:" }, tags = "@Skim")

public class TestRunner {
	private static final TargetProcess tp = new TargetProcess();
	public int testPlanRunID = 0;
	private static int testPlanId = 0;
	static String buildName = null;
	public static int buildID = -1;
	static ReusableMethods reusableMethods;
	// SharedResources sr=new SharedResources();
	// WebDriver driver=sr.init();
	private static Logger logger = Logger.getLogger(TestRunner.class);

	@BeforeClass
	public static void setup() throws InterruptedException {
		logger.info("---Execution Started---");
		try {
			//For Image comparision
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
			
			
			reusableMethods = new ReusableMethods();

			testPlanId = Integer.parseInt(reusableMethods.getTestPlanID());
			buildName = reusableMethods.getBuildName();

			System.out.println(testPlanId + " " + buildName);

			System.out.println("testPlanId:::::::::::" + testPlanId);
			System.out.println("Build Name:::::::" + buildName);

			buildID = tp.createBuild(null, testPlanId, buildName);
			System.out.println("Build Number:::::" + tp.getBuildId());

		} catch (Exception e) {
			System.out.println("Could not create the build" + e.getMessage());
		}
		ExtentProperties extentProperties = ExtentProperties.INSTANCE;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String time = sdf.format(timestamp);
		extentProperties.setReportPath("target/cucumber-reports/TsuruhaExtentReport" + time + ".html");
	}

	@AfterClass
	public static void writeExtentReport() throws Exception {
		Reporter.loadXMLConfig(new File("./resources/extent-config.xml"));
		logger.info("---Execution Complete---");
		System.gc();
	}
}
